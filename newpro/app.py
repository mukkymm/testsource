
from flask import Flask, render_template, request
from jinja2 import Template
from pythainlp.tokenize import word_tokenize


app = Flask(__name__)

    
@app.route('/pathfa')
def fname():
        text= "à¸Šà¸­à¸šà¹€à¸ˆà¹‰à¸²à¹�à¸¡à¸§à¸­à¹‰à¸§à¸‡à¹€à¸­à¹‹à¸¢"
        a = word_tokenize(text,engine='icu')
        b = word_tokenize(text,engine='newmm')
        return render_template('pathfa.html',ansA = a,ansB = b)

@app.route('/')
def Home():
    return "<center><h1>Hello World!</h1></center> "


@app.route('/index',methods=['GET','POST'])
def word():
    if request.method=='POST':
        text=request.form.get('word_old')
        answer_list =word_tokenize(text,engine='icu')
        answer_list2=word_tokenize(text,engine='mm')
        answer_list3=word_tokenize(text,engine='newmm')
        return render_template('index.html',word1 =sorted(set(answer_list), key=answer_list.index),word2 = sorted(set(answer_list2), key=answer_list2.index),word3 = sorted(set(answer_list3), key=answer_list3.index))

    return render_template('index.html')



if __name__ == '__main__':
  app.debug = True
  app.run(host='0.0.0.0', port=8000)
  app.config['TRAP_BAD_REQUEST_ERRORS'] = True
